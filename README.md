Auburn IT is veteran co-owned and glad to be your IT team, servicing the Southeast. With over 40 years of combined experience in the IT industry, we bring deep product knowledge, a comprehensive understanding of our customers’ business and solid ingenuity to every client relationship.

Address: 3365 Skyway Dr, Bldg 200, Auburn, AL 36830, USA

Phone: 334-625-1770

Website: https://www.auburnit.com
